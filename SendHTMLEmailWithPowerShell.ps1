﻿# Basic parametres
$staffhome = $env:USERPROFILE
$downloads = $staffhome + "\Downloads"

#Sends the email
ForEach($user in $list){
    $attachment = @(Get-ChildItem "$PSScriptRoot\images\")
    $smtpServer = "REDACTED.au"
    $from = "noreply@REDACTED.com.au" # This can be anything
    $domain = "@REDACTED.com.au"
    $email = $user + $domain
    $body = Get-Content "$PSScriptRoot\emailMessageHTML.txt" | Out-String
    $subject = "Your REDACTED"

    $params = @{
        To = $email
        From = $from
        SMTPServer = $smtpServer
        Subject = $subject
        BodyAsHTML = $true
        Body = $body -f ($attachment.Name)
        Attachments = $attachment.FullName
    }

    Send-MailMessage @params
    write-host "Email sent to:" $user
}

